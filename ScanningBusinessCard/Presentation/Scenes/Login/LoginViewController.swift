//
//  LoginViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/29/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginViewController: UIViewController {
    // MARK: - UI Properties
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logoImageView: UIImageView!
    
    // MARK: - Properties
    private let loginUseCase = LoginUseCase()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentOffset.y = 0
        usernameTextField.text = ""
        passwordTextField.text = ""
        hiddenKeyboard()
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        hiddenKeyboard()
        
        let username = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        if username != "", password != "" {
            loginUseCase.excute(param: LoginParam(username: username, password: password, completion: { [weak self] (error, loginResponse) in
                guard let mSelf = self else { return }
                if error != nil {
                    mSelf.showError(content: "Lỗi server")
                    return
                }
                
                SVProgressHUD.dismiss()
                guard
                    let loginResponse = loginResponse,
                    let success = loginResponse.success,
                    let message = loginResponse.message
                else { return }
                if success {
                    UserDefaults.standard.set(username, forKey: "__username__")
                    let tabbarViewController = UIStoryboard(name: Constants.tabbarName.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.tabbarID.rawValue)
                    guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return }
                    delegate.window?.rootViewController = tabbarViewController
                } else {
                    mSelf.showError(content: message)
                }
            }))
        } else {
            showError(content: "Không được để trống username hoặc password")
        }
    }
    
    @IBAction func signupButtonClicked(_ sender: Any) {
        let signUpViewController = UIStoryboard(name: Constants.login.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.signUpID.rawValue) as! SignUpViewController
        self.navigationController?.pushViewController(signUpViewController, animated: true)
    }
    
    private func hiddenKeyboard() {
        usernameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    private func config() {
        usernameTextField.config()
        passwordTextField.config()
        
        loginButton.layer.do {
            $0.cornerRadius = 5
        }
        
        signupButton.layer.do {
            $0.cornerRadius = 5
            $0.borderWidth = 1
            $0.borderColor = AppColors.red.cgColor
        }
        logoImageView.layer.do {
            $0.cornerRadius = 100
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            textField.resignFirstResponder()
        }
        return true
    }
}
