//
//  SignUpViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/29/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import SVProgressHUD

class SignUpViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    private let signUpUseCase = SignUpUseCase()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentOffset.y = 0
        usernameTextField.text = ""
        passwordTextField.text = ""
        confirmPasswordTextField.text = ""
        hiddenKeyboard()
    }
    
    @IBAction func signupButtonClicked(_ sender: Any) {
        hiddenKeyboard()
        
        let username = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        let confirmPassword = confirmPasswordTextField.text ?? ""
        if username != "", password != "", confirmPassword != "" {
            if password != confirmPassword {
                showError(content: "Mật khẩu xác nhận chưa khớp")
            } else {
                signUpUseCase.excute(param: LoginParam(username: username, password: password, completion: { [weak self] (error, signUpStatus) in
                    guard let mSelf = self else { return }
                    if error != nil {
                        mSelf.showError(content: "Lỗi error")
                        return
                    }
                    
                    SVProgressHUD.dismiss()
                    guard
                        let signUpStatus = signUpStatus,
                        let success = signUpStatus.success,
                        let message = signUpStatus.message
                    else { return }
                    if success {
                        mSelf.navigationController?.popToRootViewController(animated: true)
                    } else {
                        mSelf.showError(content: message)
                    }
                }))
            }
        } else {
            showError(content: "Không được để trống trường nào")
        }
    }
    
    private func config() {
        signupButton.layer.do {
            $0.cornerRadius = 5
        }
        
        usernameTextField.config()
        passwordTextField.config()
        confirmPasswordTextField.config()
    }
    
    private func hiddenKeyboard() {
        usernameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        confirmPasswordTextField.resignFirstResponder()
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            confirmPasswordTextField.becomeFirstResponder()
        } else if textField == confirmPasswordTextField {
            textField.resignFirstResponder()
        }
        
        return true
    }
}
