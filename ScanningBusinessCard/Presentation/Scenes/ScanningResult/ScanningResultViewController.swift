//
//  ScanningResultViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import MessageUI
import SVProgressHUD
import FontAwesome_swift
import ContactsUI

enum ResultViewType {
    case see
    case edited
}

class ScanningResultViewController: UIViewController {
    // MARK: - UI Properties
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var cardTitleLabel: UILabel!
    @IBOutlet weak var cardOrganizationLabel: UILabel!
    @IBOutlet weak var cardPhoneLabel: UILabel!
    @IBOutlet weak var cardEmailLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var sendMessageButton: UIButton!
    @IBOutlet weak var sendEmailButton: UIButton!
    @IBOutlet weak var emailTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var saveButtonTopConstaint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var saveLocalButton: UIBarButtonItem!
    
    // MARK: - Local properties
    var cardUIImage: UIImage?
    var cardId = ""
    var cardName = ""
    var cardTitle = ""
    var cardOrganization = ""
    var cardMobile = ""
    var cardEmail = ""
    var type: ResultViewType = .see
    var isFirstTime = false
    
    // UseCase
    private let deleteCardUseCase = DeleteCardUseCase()
    private let updateCardUseCase = UpdateCardUseCase()
    private let createNewCardUseCase = CreateNewCardUseCase()
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    // MARK: - Actions
    @IBAction func cancelClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func editClicked(_ sender: Any) {
        let editScanningResultViewController = Storyboards.editScanningResultStoryboard.instantiateViewController(withIdentifier: Constants.editScanningResultID.rawValue) as! EditScanningResultViewController
        editScanningResultViewController.delegate = self
        editScanningResultViewController.setContent(card: Card(
            cardId: cardId,
            name: cardName,
            title: cardTitle,
            organization: cardOrganization,
            mobile: cardMobile,
            email: cardEmail)
        )
        self.navigationController?.pushViewController(editScanningResultViewController, animated: true)
    }
    
    @IBAction func saveLocalButtonClicked(_ sender: Any) {
        pushContactUI()
    }
    
    @IBAction func saveClicked(_ sender: Any) {
        if isFirstTime {
            let controller = UIAlertController(title: "Chọn nơi lưu", message: "", preferredStyle: .actionSheet)
            let localSave = UIAlertAction(title: "Lưu vào máy", style: .default) { [weak self] _ in
                guard let mSelf = self else { return }
                mSelf.pushContactUI()
            }
            let cloudSave = UIAlertAction(title: "Lưu vào ứng dụng", style: .default) { [weak self] _ in
                guard let mSelf = self else { return }
                mSelf.createNewCardUseCase.excute(param: CreateNewCardParam(card: Card(cardId: UUID().uuidString.replacingOccurrences(of: "-", with: ""), name: mSelf.cardName, title: mSelf.cardTitle, organization: mSelf.cardOrganization, mobile: mSelf.cardMobile, email: mSelf.cardEmail), completion: { [weak self] (error, response) in
                    guard let mSelf = self else { return }
                    if error != nil {
                        mSelf.showError(content: "Lỗi server")
                        return
                    }
                    
                    SVProgressHUD.dismiss()
                   
                    guard
                        let response = response,
                        let success = response.success
                    else { return }
                    if !success {
                        guard let message = response.message else { return }
                        mSelf.showError(content: message)
                    }
                    
                    mSelf.navigationController?.popViewController(animated: true)
                }))
            }
            let cancel = UIAlertAction(title: "Thoát", style: .destructive) { _ in
                controller.dismiss(animated: true, completion: nil)
            }
            controller.addAction(localSave)
            controller.addAction(cloudSave)
            controller.addAction(cancel)
            controller.view.tintColor = AppColors.red
            self.present(controller, animated: true, completion: nil)
        } else {
            updateCardUseCase.excute(param: UpdateCardParam(card: Card(cardId: cardId, name: cardName, title: cardTitle, organization: cardOrganization, mobile: cardMobile, email: cardEmail), completion: { [weak self] (error, response) in
                guard let mSelf = self else { return }
                if error != nil {
                    mSelf.showError(content: "Lỗi server")
                    return
                }
                
                SVProgressHUD.dismiss()
               
                guard
                    let response = response,
                    let success = response.success
                else { return }
                if !success {
                    guard let message = response.message else { return }
                    mSelf.showError(content: message)
                }
                mSelf.navigationController?.popViewController(animated: true)
            }))
        }
    }
    
    @IBAction func deleteClicked(_ sender: Any) {
        let controller = UIAlertController(title: "Cảnh báo", message: "Bạn có chắc chắn muốn xoá danh thiếp này không", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] _ in
            guard let mSelf = self else { return }
            mSelf.deleteCardUseCase.excute(param: DeleteCardParam(cardId: mSelf.cardId, completion: { [weak self] (error, response) in
                guard let mSelf = self else { return }
                if error != nil {
                    mSelf.showError(content: "Lỗi server")
                    return
                }
                
                SVProgressHUD.dismiss()
               
                guard
                    let response = response,
                    let success = response.success
                else { return }
                if !success {
                    guard let message = response.message else { return }
                    mSelf.showError(content: message)
                }
                mSelf.navigationController?.popViewController(animated: true)
            }))
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        controller.addAction(okAction)
        controller.addAction(cancelAction)
        controller.view.tintColor = AppColors.red
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func callButtonClicked(_ sender: Any) {
        makeAPhoneCall()
    }
    
    @IBAction func sendMessageButtonClicked(_ sender: Any) {
        sendSMSText()
    }
    
    @IBAction func sendEmailButtonClicked(_ sender: Any) {
        sendEmail()
    }
    
    
    // MARK: - Functions
    private func config() {
        emailTopConstraint.constant = cardMobile == "Trống" ? 20.0 : 58.0
        saveButtonTopConstaint.constant = cardEmail == "Trống" ? 40.0 : 78.0
        
        callButton.layer.do { [weak self] in
            guard let mSelf = self else { return }
            $0.isHidden = mSelf.cardMobile == "Trống"
            $0.borderWidth = 1
            $0.borderColor = AppColors.red.cgColor
            $0.cornerRadius = 5
        }
        
        sendMessageButton.layer.do { [weak self] in
            guard let mSelf = self else { return }
            $0.isHidden = mSelf.cardMobile == "Trống"
            $0.borderWidth = 1
            $0.borderColor = AppColors.red.cgColor
            $0.cornerRadius = 5
        }
        
        sendEmailButton.layer.do { [weak self] in
            guard let mSelf = self else { return }
            $0.isHidden = mSelf.cardEmail == "Trống"
            $0.borderWidth = 1
            $0.borderColor = AppColors.red.cgColor
            $0.cornerRadius = 5
        }
        
        saveButton.layer.cornerRadius = 5
        deleteButton.layer.do {
            $0.cornerRadius = 5
            $0.borderWidth = 1
            $0.borderColor = AppColors.red.cgColor
        }
        if let cardUIImage = cardUIImage {
            cardImage.image = cardUIImage
        }
        cardNameLabel.text = cardName
        cardTitleLabel.text = cardTitle
        cardOrganizationLabel.text = cardOrganization
        cardPhoneLabel.text = cardMobile
        cardEmailLabel.text = cardEmail
        
        saveButton.isHidden = type == .see
        deleteButton.isHidden = isFirstTime
        
        cancelButton.image = UIImage.fontAwesomeIcon(name: .windowClose, style: .regular, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
        
        editButton.image = UIImage.fontAwesomeIcon(name: .edit, style: .regular, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
        
        saveLocalButton.image = UIImage.fontAwesomeIcon(name: .shareSquare, style: .regular, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
    }
    
    private func pushContactUI() {
        let contact = CNMutableContact()

        var cardNameArr = cardName.split(separator: " ")
        contact.givenName = String(cardNameArr[cardNameArr.count-1])
        cardNameArr.removeLast()
        contact.familyName = cardNameArr.joined(separator: " ")
        contact.jobTitle = cardTitle
        contact.organizationName = cardOrganization
        contact.emailAddresses.append(CNLabeledValue(label: "email", value: NSString(string: cardEmail)))
        contact.phoneNumbers.append(CNLabeledValue(label: "mobile", value: CNPhoneNumber(stringValue: cardMobile)))

        let contactViewController = CNContactViewController(forUnknownContact: contact)
        contactViewController.contactStore = CNContactStore()
        contactViewController.view.tintColor = AppColors.red
        navigationController?.pushViewController(contactViewController, animated: true)
    }
    
    private func makeAPhoneCall() {
        guard
            let url = URL(string: "tel://\(cardMobile)")
            else { return }
        UIApplication.shared.openURL(url)
    }
    
    private func sendSMSText() {
        if MFMessageComposeViewController.canSendText() {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = [cardMobile]
            controller.messageComposeDelegate = self
            controller.view.tintColor = AppColors.red
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    private func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let controller = MFMailComposeViewController()
            controller.mailComposeDelegate = self
            controller.setToRecipients([cardEmail])
            controller.setMessageBody("", isHTML: false)
            controller.view.tintColor = AppColors.red
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func setContent(type: ResultViewType, image: UIImage?, card: Card) {
        if image != nil {
            self.cardUIImage = image
        }
        self.cardId = card.cardId ?? "Trống"
        self.cardName = card.name ?? "Trống"
        self.cardTitle = card.title ?? "Trống"
        self.cardOrganization = card.organization ?? "Trống"
        self.cardMobile = card.mobile ?? "Trống"
        self.cardEmail = card.email ?? "Trống"
        self.type = type
    }
}

extension ScanningResultViewController: EditingDoneDelegate {
    func finish(card: Card) {
        guard
            let name = card.name,
            let title = card.title,
            let organization = card.organization,
            let mobile = card.mobile,
            let email = card.email
        else { return }
        self.cardName = name.isEmpty ? "Trống" : name
        self.cardTitle = title.isEmpty ? "Trống" : title
        self.cardOrganization = organization.isEmpty ? "Trống" : organization
        self.cardMobile = mobile.isEmpty ? "Trống" : mobile
        self.cardEmail = email.isEmpty ? "Trống" : email
        self.type = .edited
        config()
    }
}

extension ScanningResultViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
        switch result {
        case .cancelled: break
        case .failed:
            SVProgressHUD.showError(withStatus: "Gửi tin nhắn lỗi")
        case .sent:
            SVProgressHUD.showSuccess(withStatus: "Gửi tin nhắn thành công")
        @unknown default:
            SVProgressHUD.showError(withStatus: "Lỗi không xác định")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension ScanningResultViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if error != nil {
            SVProgressHUD.showError(withStatus: error?.localizedDescription)
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        switch result {
            case .cancelled: break
            case .failed:
                SVProgressHUD.showError(withStatus: "Gửi email lỗi")
            case .sent:
                SVProgressHUD.showSuccess(withStatus: "Gửi email thành công")
            case .saved:
                SVProgressHUD.showSuccess(withStatus: "Lưu email nháp thành công")
            @unknown default:
                SVProgressHUD.showError(withStatus: "Lỗi không xác định")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}

