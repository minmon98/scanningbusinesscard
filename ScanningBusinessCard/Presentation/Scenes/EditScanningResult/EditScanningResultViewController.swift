//
//  EditScanningResultViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 2/2/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import Then
import FontAwesome_swift

protocol EditingDoneDelegate: class {
    func finish(card: Card)
}

class EditScanningResultViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var organizationTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var confirmButton: UIBarButtonItem!
    
    var cardId = ""
    var cardName = ""
    var cardTitle = ""
    var cardOrganization = ""
    var cardPhone = ""
    var cardEmail = ""
    var delegate: EditingDoneDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        guard let dlg = delegate else { return }
        dlg.finish(card: Card(
            cardId: cardId,
            name: nameTextField.text ?? "",
            title: titleTextField.text ?? "",
            organization: organizationTextField.text ?? "",
            mobile: phoneTextField.text ?? "",
            email: emailTextField.text ?? ""
        ))
        self.navigationController?.popViewController(animated: true)
    }
    
    private func config() {
        self.nameTextField.do {
            $0.text = cardName == "Trống" ? "" : cardName
            $0.config()
        }
        self.titleTextField.do {
            $0.text = cardTitle == "Trống" ? "" : cardTitle
            $0.config()
        }
        self.organizationTextField.do {
            $0.text = cardOrganization == "Trống" ? "" : cardOrganization
            $0.config()
        }
        self.phoneTextField.do {
            $0.text = cardPhone == "Trống" ? "" : cardPhone
            $0.config()
        }
        self.emailTextField.do {
            $0.text = cardEmail == "Trống" ? "" : cardEmail
            $0.config()
        }
        self.backButton.image = UIImage.fontAwesomeIcon(name: .caretSquareLeft, style: .regular, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
        self.confirmButton.image = UIImage.fontAwesomeIcon(name: .checkSquare, style: .regular, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
    }
    
    func setContent(card: Card) {
        self.cardId = card.cardId ?? ""
        self.cardName = card.name ?? ""
        self.cardTitle = card.title ?? ""
        self.cardOrganization = card.organization ?? ""
        self.cardPhone = card.mobile ?? ""
        self.cardEmail = card.email ?? ""
    }
}

extension EditScanningResultViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            titleTextField.becomeFirstResponder()
        } else if textField == titleTextField {
            organizationTextField.becomeFirstResponder()
        } else if textField == organizationTextField {
            phoneTextField.becomeFirstResponder()
        } else if textField == phoneTextField {
            emailTextField.becomeFirstResponder()
        } else if textField == emailTextField {
            textField.resignFirstResponder()
            guard let dlg = delegate else { return true }
            dlg.finish(card: Card(
                cardId: cardId,
                name: nameTextField.text ?? "",
                title: titleTextField.text ?? "",
                organization: organizationTextField.text ?? "",
                mobile: phoneTextField.text ?? "",
                email: emailTextField.text ?? ""
            ))
            self.navigationController?.popViewController(animated: true)
        }
        return true
    }
}
