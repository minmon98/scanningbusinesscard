//
//  NewCardCell.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import Reusable
import FontAwesome_swift

class NewCardCell: UITableViewCell, NibReusable {
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var cardTitleLabel: UILabel!
    @IBOutlet weak var cardOrganizationLabel: UILabel!
    @IBOutlet weak var cardMobileLabel: UILabel!
    @IBOutlet weak var cardEmailLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardBackgroundView: UIView!
    @IBOutlet weak var cardBlurBackgroundView: UIView!
    @IBOutlet weak var removeButton: UIButton!
    
    var onClickedCard: ((NewCardCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        config()
    }
    
    @IBAction func onClicked(_ sender: Any) {
        if let onClick = onClickedCard {
            onClick(self)
        }
    }
    
    private func config() {
        self.cardView.layer.cornerRadius = 10
        self.cardBackgroundView.layer.cornerRadius = 10
        self.cardBlurBackgroundView.layer.cornerRadius = 10
        self.removeButton.setBackgroundImage(UIImage.fontAwesomeIcon(name: .trash, style: .solid, textColor: .white, size: CGSize(width: 30, height: 30)), for: .normal)
    }
    
    func setContent(card: Card) {
        self.cardNameLabel.text = card.name ?? ""
        self.cardTitleLabel.text = card.title ?? ""
        self.cardOrganizationLabel.text = card.organization ?? ""
        self.cardMobileLabel.text = card.mobile ?? ""
        self.cardEmailLabel.text = card.email ?? ""
    }
}
