//
//  ConfirmNewCardsViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import FontAwesome_swift
import SVProgressHUD

class ConfirmNewCardsViewController: UIViewController {
    @IBOutlet weak var cardTableView: UITableView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIButton!
    
    var listOfCards = [Card]()
    private let mergeCardsUseCase = MergeCardsUseCase()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonClicked(_ sender: Any) {
        mergeCardsUseCase.excute(param: MergeCardsParam(cards: listOfCards, completion: { [weak self] (error, response) in
            guard let mSelf = self else { return }
            if error != nil {
                mSelf.showError(content: "Lỗi server")
            }
            
            SVProgressHUD.dismiss()
            guard
                let response = response,
                let success = response.success
            else { return }
            if success {
                guard let message = response.message else { return }
                SVProgressHUD.showSuccess(withStatus: message)
                mSelf.navigationController?.popViewController(animated: true)
            }
        }))
    }
    
    private func checkEnableSaveButton() {
        if listOfCards.isEmpty {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    private func config() {
        backButton.image = UIImage.fontAwesomeIcon(name: .caretSquareLeft, style: .regular, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
        cardTableView.do {
            $0.dataSource = self
            $0.delegate = self
            $0.register(cellType: NewCardCell.self)
        }
        saveButton.layer.cornerRadius = 5
    }
}

extension ConfirmNewCardsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as NewCardCell
        cell.setContent(card: listOfCards[indexPath.row])
        cell.onClickedCard = { [weak self] _ in
            guard let mSelf = self else { return }
            mSelf.listOfCards.remove(at: indexPath.row)
            mSelf.cardTableView.reloadData()
            mSelf.checkEnableSaveButton()
        }
        return cell
    }
}
