//
//  SettingViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/1/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import FontAwesome_swift

class SettingViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    @IBAction func logoutButtonClicked(_ sender: Any) {
        let controller = UIAlertController(title: "Cảnh báo", message: "Bạn có chắc chắn muốn đăng xuất không", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            UserDefaults.standard.set("", forKey: "__username__")
            let loginViewController = UIStoryboard(name: Constants.login.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.loginID.rawValue)
            guard let delegate = UIApplication.shared.delegate as? AppDelegate else { return }
            delegate.window?.rootViewController = loginViewController
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        controller.addAction(okAction)
        controller.addAction(cancelAction)
        controller.view.tintColor = AppColors.red
        self.present(controller, animated: true, completion: nil)
    }
    
    private func config() {
        usernameTextField.text = UserDefaults.standard.string(forKey: "__username__") ?? ""
        
        userImageView.image = UIImage.fontAwesomeIcon(name: .userCircle, style: .solid, textColor: AppColors.red, size: CGSize(width: 200, height: 200))
        
        logoutButton.layer.do {
            $0.cornerRadius = 5
            $0.borderWidth = 1
            $0.borderColor = AppColors.red.cgColor
        }
    }
}
