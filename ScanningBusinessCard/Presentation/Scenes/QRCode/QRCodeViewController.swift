//
//  QRCodeViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import Reusable
import QRCodeReader
import SVProgressHUD

class QRCodeViewController: UIViewController {
    @IBOutlet weak var cardTableView: UITableView!
    @IBOutlet weak var generateQRCodeView: UIView!
    @IBOutlet weak var generateQRCodeButton: UIButton!
    @IBOutlet weak var qrCodeButton: UIBarButtonItem!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBarHeightConstraint: NSLayoutConstraint!
    
    private let getCardUseCase = GetCardUseCase()
    var listOfCards = [(Bool, Card)]()
    private var listOfCardsTempt = [(Bool, Card)]()
    let getAllCardsUseCase = GetAllCardsUseCase()
    let generateQRCodeUseCase = GenerateQRCodeUseCase()
    private var isReset = true
    private var isShowSearchBar = false
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            
            // Configure the view controller (optional)
            $0.showTorchButton        = true
            $0.showSwitchCameraButton = true
            $0.showCancelButton       = true
            $0.showOverlayView        = true
            $0.rectOfInterest         = CGRect(x: 0.2, y: 0.2, width: 0.6, height: 0.6)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isReset {
            isShowSearchBar = false
            receiveData()
            config()
        } else {
            isReset = true
        }
    }
    
    @IBAction func generateQRButtonClicked(_ sender: Any) {
        generateQRCode()
    }
    
    @IBAction func searchButtonClicked(_ sender: Any) {
        isShowSearchBar = !isShowSearchBar
        config()
    }
    
    @IBAction func searchChangeValue(_ sender: UITextField) {
        let content = (sender.text ?? "").lowercased()
        listOfCards.removeAll()
        for element in listOfCardsTempt {
            let card = element.1
            let name = (card.name ?? "").lowercased()
            let mobile = (card.mobile ?? "").lowercased()
            let email = (card.email ?? "").lowercased()
            let organization = (card.organization ?? "").lowercased()
            if name.contains(content) || mobile.contains(content) || email.contains(content) || organization.contains(content) {
                listOfCards.append((false, card))
            }
        }
        self.cardTableView.reloadData()
    }
    
    @IBAction func qrCodeButtonClicked(_ sender: Any) {
        self.present(readerVC, animated: true, completion: nil)
    }
    
    private func generateQRCode() {
        var cardIds = [String]()
        let searchContent = searchTextField.text ?? ""
        if searchContent.isEmpty {
            for element in listOfCardsTempt {
                if element.0 {
                    cardIds.append(element.1.cardId ?? "")
                }
            }
        } else {
            for element in listOfCards {
                if element.0 {
                    cardIds.append(element.1.cardId ?? "")
                }
            }
        }
        generateQRCodeUseCase.excute(param: GenerateQRCodeParam(cardIds: cardIds, completion: { [weak self] in
            guard let mSelf = self else { return }
            if $0 != nil {
                mSelf.showError(content: "Lỗi server")
                return
            }
            
            SVProgressHUD.dismiss()
            guard
                let generateQRCodeResponse = $1,
                let url = generateQRCodeResponse.url
            else { return }
            let qrCodeImageViewController = UIStoryboard(name: Constants.qrCodeName.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.qrCodeImageID.rawValue) as! QRCodeImageViewController
            qrCodeImageViewController.urlString = url
            mSelf.navigationController?.pushViewController(qrCodeImageViewController, animated: true)
        }))
    }
    
    private func receiveData() {
        getAllCardsUseCase.excute(param: GetAllCardsParam(
            username: UserDefaults.standard.string(forKey: "__username__") ?? "",
            completion: { [weak self] in
                guard let mSelf = self else { return }
                if $0 != nil {
                    mSelf.showError(content: "Lỗi server")
                    return
                }
                
                SVProgressHUD.dismiss()
                guard
                    let getAllCardsResponse = $1,
                    let success = getAllCardsResponse.success
                else { return }
                if success {
                    guard let cards = getAllCardsResponse.cards else { return }
                    mSelf.listOfCards = cards.map({
                        return (false, $0)
                    })
                    mSelf.listOfCardsTempt = mSelf.listOfCards
                    mSelf.cardTableView.reloadData()
                } else {
                    guard let message = getAllCardsResponse.message else { return }
                    mSelf.showError(content: message)
                }
        }))
    }
    
    private func config() {
        cardTableView.do {
            $0.dataSource = self
            $0.delegate = self
            $0.register(cellType: QRCodeCell.self)
        }
        generateQRCodeView.isHidden = true
        generateQRCodeButton.do {
            $0.isHidden = true
            $0.layer.cornerRadius = 5
        }
        qrCodeButton.image = UIImage.fontAwesomeIcon(name: .qrcode, style: .solid, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
        readerVC.do {
            $0.delegate = self
            $0.modalPresentationStyle = .formSheet
        }
        
        searchButton.image = UIImage.fontAwesomeIcon(name: .search, style: .solid, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
        
        searchTextField.text = ""
        searchBarHeightConstraint.constant = isShowSearchBar ? 50.0 : 0.0
        if !isShowSearchBar {
            searchTextField.resignFirstResponder()
            self.cardTableView.reloadData()
        } else {
            searchTextField.becomeFirstResponder()
        }
    }
    
    private func checkEnableGenerateQRCodeButton() {
        let searchContent = searchTextField.text ?? ""
        if searchContent.isEmpty {
            for element in listOfCardsTempt {
                if element.0 {
                    generateQRCodeButton.isHidden = false
                    generateQRCodeView.isHidden = false
                    return
                }
            }
        } else {
            for element in listOfCards {
                if element.0 {
                    generateQRCodeButton.isHidden = false
                    generateQRCodeView.isHidden = false
                    return
                }
            }
        }
        generateQRCodeButton.isHidden = true
        generateQRCodeView.isHidden = true
    }
}

extension QRCodeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let searchContent = searchTextField.text ?? ""
        return searchContent.isEmpty ? listOfCardsTempt.count : listOfCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchContent = searchTextField.text ?? ""
        let cell = tableView.dequeueReusableCell(for: indexPath) as QRCodeCell
        let (_, card) = searchContent.isEmpty ? listOfCardsTempt[indexPath.row] : listOfCards[indexPath.row]
        cell.setContent(card: card)
        cell.onClickedCard = { [weak self] qrCodeCell in
            guard let mSelf = self else { return }
            if searchContent.isEmpty {
                mSelf.listOfCardsTempt[indexPath.row].0 = qrCodeCell.selectedCell
            } else {
                mSelf.listOfCards[indexPath.row].0 = qrCodeCell.selectedCell
            }
            mSelf.checkEnableGenerateQRCodeButton()
        }
        return cell
    }
}

extension QRCodeViewController: QRCodeReaderViewControllerDelegate {
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        getCardUseCase.excute(param: GetCardParam(url: result.value, completion: { [weak self] in
            guard let mSelf = self else { return }
            if $0 != nil {
                mSelf.showError(content: "QR Code này không hỗ trợ")
                return
            }
            
            SVProgressHUD.dismiss()
            guard
                let getAllCardsResponse = $1,
                let cards = getAllCardsResponse.cards
            else { return }
            let confirmNewCardsViewController = UIStoryboard(name: Constants.confirmNewCardsName.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.confirmNewCardsID.rawValue) as! ConfirmNewCardsViewController
            confirmNewCardsViewController.listOfCards = cards
            mSelf.navigationController?.pushViewController(confirmNewCardsViewController, animated: true)
        }))
        
        reader.stopScanning()
        isReset = false
        dismiss(animated: true, completion: nil)
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        isReset = false
        dismiss(animated: true, completion: nil)
    }
}

extension QRCodeViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let content = (textField.text ?? "").lowercased()
        listOfCards.removeAll()
        for element in listOfCardsTempt {
            let card = element.1
            let name = (card.name ?? "").lowercased()
            let mobile = (card.mobile ?? "").lowercased()
            let email = (card.email ?? "").lowercased()
            let organization = (card.organization ?? "").lowercased()
            if name.contains(content) || mobile.contains(content) || email.contains(content) || organization.contains(content) {
                listOfCards.append((false, card))
            }
        }
        self.cardTableView.reloadData()
        textField.resignFirstResponder()
        return true
    }
}

