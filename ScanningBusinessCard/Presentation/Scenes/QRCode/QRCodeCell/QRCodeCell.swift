//
//  QRCodeCell.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import Reusable
import FontAwesome_swift

class QRCodeCell: UITableViewCell, NibReusable {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var checkedButton: UIButton!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var cardTitleLabel: UILabel!
    @IBOutlet weak var cardPhoneLabel: UILabel!
    @IBOutlet weak var cardEmailLabel: UILabel!
    var onClickedCard: ((QRCodeCell) -> Void)?
    var selectedCell = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.layer.cornerRadius = 10
        config()
    }
    
    @IBAction func onClicked(_ sender: Any) {
        if let onClick = onClickedCard {
            selectedCell = !selectedCell
            config()
            onClick(self)
        }
    }
    
    private func config() {        checkedButton.setBackgroundImage(UIImage.fontAwesomeIcon(name: .checkCircle, style: selectedCell ? .solid : .regular, textColor: AppColors.red, size: CGSize(width: 30, height: 30)), for: .normal)
    }
    
    func setContent(card: Card) {
        selectedCell = false
        config()
        self.cardNameLabel.text = card.name
        self.cardTitleLabel.text = card.title
        self.cardPhoneLabel.text = card.mobile
        self.cardEmailLabel.text = card.email
    }
}
