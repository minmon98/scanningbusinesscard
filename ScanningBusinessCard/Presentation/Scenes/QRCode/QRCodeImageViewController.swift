//
//  QRCodeImageViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import FontAwesome_swift

class QRCodeImageViewController: UIViewController {
    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    var urlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        downloadImage()
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func downloadImage() {
        guard let url = URL(string: urlString) else { return }
        do {
            let data = try Data(contentsOf: url)
            qrImageView.image = UIImage(data: data)
        } catch let error {
            showError(content: error.localizedDescription)
        }
    }
    
    private func config() {
        backButton.image = UIImage.fontAwesomeIcon(name: .caretSquareLeft, style: .regular, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
    }
}
