//
//  TabbarController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/1/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import FontAwesome_swift

class TabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    private func config() {
        self.delegate = self
        tabBar.tintColor = AppColors.red
        
        let listOfCardsViewController = UIStoryboard(name: Constants.listOfCardsName.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.listOfCardsID.rawValue)
        
        let qrCodeViewController = UIStoryboard(name: Constants.qrCodeName.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.qrCodeID.rawValue)
        
        let settingViewController = UIStoryboard(name: Constants.settingName.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.settingID.rawValue)
        
        listOfCardsViewController.tabBarItem = UITabBarItem(title: "Home", image: UIImage.fontAwesomeIcon(name: .home, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30)), tag: 0)
        
        qrCodeViewController.tabBarItem = UITabBarItem(title: "QR Code", image: UIImage.fontAwesomeIcon(name: .qrcode, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30)), tag: 1)
        
        settingViewController.tabBarItem = UITabBarItem(title: "Setting", image: UIImage.fontAwesomeIcon(name: .cog, style: .solid, textColor: .black, size: CGSize(width: 30, height: 30)), tag: 2)
        
        self.viewControllers = [listOfCardsViewController, qrCodeViewController, settingViewController]
    }
}

extension TabbarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let viewController = viewController as? UINavigationController {
            viewController.popToRootViewController(animated: true)
        }
    }
}
