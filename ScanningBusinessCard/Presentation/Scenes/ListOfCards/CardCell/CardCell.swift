//
//  CardCell.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 2/2/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import Reusable
import Then

class CardCell: UITableViewCell, NibReusable {
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardNameLabel: UILabel!
    @IBOutlet weak var cardTitleLabel: UILabel!
    @IBOutlet weak var cardPhoneLabel: UILabel!
    @IBOutlet weak var cardEmailLabel: UILabel!
    
    var onClickedCard: ((CardCell) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        config()
    }
    
    @IBAction func onClicked(_ sender: Any) {
        if let onClick = onClickedCard {
            onClick(self)
        }
    }
    
    private func config() {
        cardView.layer.cornerRadius = 10
    }
    
    func setContent(image: UIImage, card: Card) {
        self.cardNameLabel.text = card.name
        self.cardTitleLabel.text = card.title
        self.cardPhoneLabel.text = card.mobile
        self.cardEmailLabel.text = card.email
    }
}
