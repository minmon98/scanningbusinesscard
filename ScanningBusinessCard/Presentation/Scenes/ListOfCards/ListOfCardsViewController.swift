//
//  ViewController.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/11/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import AVFoundation
import Then
import SVProgressHUD
import FirebaseMLVision
import CoreData
import CLImageEditor
import FontAwesome_swift

enum ImageSource {
    case camera
    case library
    
    func setting(picker: UIImagePickerController) {
        switch self {
            case .camera:
                picker.sourceType = .camera
            case .library:
                picker.sourceType = .photoLibrary
        }
    }
}

class ListOfCardsViewController: UIViewController {
    // MARK: - UI Properties
    @IBOutlet weak var cardTableView: UITableView!
    @IBOutlet weak var addNewCardButton: UIButton!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchBarHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Local properties
    private let getCardUseCase = GetCardUseCase()
    private let getExtractInformationUseCase = GetExtractInformationUseCase()
    private let pickerController = UIImagePickerController()
    private var listOfCards = [(UIImage, Card)]()
    private var filterCards = [(UIImage, Card)]()
    private var listOfCardsTempt = [(UIImage, Card)]()
    private let getAllCardsUseCase = GetAllCardsUseCase()
    private var isReset = true
    private var isShowSearchBar = false
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isReset {
            isShowSearchBar = false
            config()
            receiveData()
        } else {
            isReset = true
        }
    }
    
    // MARK: - Functions
    private func receiveData() {
        getAllCardsUseCase.excute(param: GetAllCardsParam(
            username: UserDefaults.standard.string(forKey: "__username__") ?? "",
            completion: { [weak self] in
                guard let mSelf = self else { return }
                if $0 != nil {
                    mSelf.showError(content: "Lỗi server")
                    return
                }
                
                SVProgressHUD.dismiss()
                guard
                    let getAllCardsResponse = $1,
                    let success = getAllCardsResponse.success
                else { return }
                if success {
                    guard let cards = getAllCardsResponse.cards else { return }
                    mSelf.listOfCards = cards.map({
                        return (UIImage.fontAwesomeIcon(name: .addressCard, style: .solid, textColor: AppColors.red, size: CGSize(width: 800, height: 400)), $0)
                    })
                    mSelf.listOfCardsTempt = mSelf.listOfCards
                    mSelf.cardTableView.reloadData()
                } else {
                    guard let message = getAllCardsResponse.message else { return }
                    mSelf.showError(content: message)
                }
        }))
    }
    
    private func config() {
        pickerController.do {
            $0.delegate = self
        }
        cardTableView.do {
            $0.dataSource = self
            $0.delegate = self
            $0.register(cellType: CardCell.self)
        }
        addNewCardButton.setBackgroundImage(UIImage.fontAwesomeIcon(name: .plusCircle, style: .solid, textColor: AppColors.red, size: CGSize(width: 50, height: 50)), for: .normal)
        
        searchButton.image = UIImage.fontAwesomeIcon(name: .search, style: .solid, textColor: AppColors.red, size: CGSize(width: 30, height: 30))
        
        searchTextField.text = ""
        searchBarHeightConstraint.constant = isShowSearchBar ? 50.0 : 0.0
        if !isShowSearchBar {
            searchTextField.resignFirstResponder()
            self.cardTableView.reloadData()
        } else {
            searchTextField.becomeFirstResponder()
        }
    }
    
    // MARK: - Actions
    @IBAction func searchButtonClicked(_
        sender: Any) {
        isShowSearchBar = !isShowSearchBar
        config()
    }
    @IBAction func searchBarChangeValue(_ sender: UITextField) {
        let content = sender.text ?? ""
        listOfCards.removeAll()
        for element in listOfCardsTempt {
            let card = element.1
            let name = (card.name ?? "").lowercased()
            let mobile = (card.mobile ?? "").lowercased()
            let email = (card.email ?? "").lowercased()
            let organization = (card.organization ?? "").lowercased()
            if name.contains(content) || mobile.contains(content) || email.contains(content) || organization.contains(content) {
                listOfCards.append((UIImage.fontAwesomeIcon(name: .addressCard, style: .solid, textColor: AppColors.red, size: CGSize(width: 800, height: 400)), card))
            }
        }
        self.cardTableView.reloadData()
    }
    
    @IBAction func chooseImageButtonClicked(_ sender: Any) {
        let controller = UIAlertController(title: "Chọn chế độ", message: "", preferredStyle: .actionSheet)
        let libraryAction = UIAlertAction(title: "Thư viện ảnh", style: .default) { [weak self] _ in
            guard let mSelf = self else { return }
            ImageSource.library.setting(picker: mSelf.pickerController)
            mSelf.present(mSelf.pickerController, animated: true, completion: nil)
        }
        let cameraAction = UIAlertAction(title: "Chụp ảnh", style: .default) { [weak self] _ in
            guard let mSelf = self else { return }
            ImageSource.camera.setting(picker: mSelf.pickerController)
            mSelf.present(mSelf.pickerController, animated: true, completion: nil)
        }
        let cancel = UIAlertAction(title: "Thoát", style: .destructive) { _ in
            controller.dismiss(animated: true, completion: nil)
        }
        controller.addAction(libraryAction)
        controller.addAction(cameraAction)
        controller.addAction(cancel)
        controller.view.tintColor = AppColors.red
        pickerController.view.tintColor = AppColors.red
        self.present(controller, animated: true, completion: nil)
    }
}

extension ListOfCardsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard
            let image = info[.originalImage] as? UIImage,
            let editor = CLImageEditor(image: image)
        else { return }
        editor.delegate = self
        editor.view.tintColor = AppColors.red
        isReset = false
        pickerController.dismiss(animated: true, completion: nil)
        present(editor, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isReset = false
        pickerController.dismiss(animated: true, completion: nil)
    }
    
    func textRecognizer(image: UIImage) {
        let vision = Vision.vision()
        let deviceTextRecognizer = vision.onDeviceTextRecognizer()
        let visionImage = VisionImage(image: image)
        
        SVProgressHUD.setBackgroundColor(UIColor.black)
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.setForegroundColor(AppColors.red)
        SVProgressHUD.show(withStatus: "Đang tách chuỗi từ ảnh")
        deviceTextRecognizer.process(visionImage) { [weak self] (result, error) in
            guard let mSelf = self else { return }
            if error != nil {
                mSelf.showError(content: error?.localizedDescription ?? "Error")
                return
            }
            
            guard let result = result else {
                SVProgressHUD.dismiss()
                mSelf.showError(content: "Không tìm thấy chuỗi trong ảnh")
                return
            }
            let text = result.text
            if text.isEmpty {
                SVProgressHUD.dismiss()
                mSelf.showError(content: "Không tìm thấy chuỗi trong ảnh")
                return
            }
            var lines = [String]()
            
            print(text)
            for block in result.blocks {
                for line in block.lines {
                    let lineText = line.text
                    lines.append(lineText)
                }
            }
            
            mSelf.getExtractInformationUseCase.excute(param: GetExtractInformationParam(text: text, lines: lines, completion: { [weak self] in
                guard let mSelf = self else { return }
                if $0 != nil {
                    mSelf.showError(content: "Lỗi server")
                    return
                }
                
                guard
                    let value = $1,
                    let card = value.result
                else { return }
                let scanningResultViewController = Storyboards.scanningResultStoryboard.instantiateViewController(withIdentifier: Constants.scanningResultID.rawValue) as! ScanningResultViewController
                scanningResultViewController.setContent(type: .edited, image: image, card: card)
                scanningResultViewController.isFirstTime = true
                mSelf.navigationController?.pushViewController(scanningResultViewController, animated: true)
                SVProgressHUD.dismiss()
            }))
        }
    }
}

extension ListOfCardsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let searchContent = searchTextField.text ?? ""
        return searchContent.isEmpty ? listOfCardsTempt.count : listOfCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchContent = searchTextField.text ?? ""
        let cell = tableView.dequeueReusableCell(for: indexPath) as CardCell
        let (image, card) = searchContent.isEmpty ? listOfCardsTempt[indexPath.row] : listOfCards[indexPath.row]
        cell.setContent(image: image, card: card)
        cell.onClickedCard = { [weak self] _ in
            guard let mSelf = self else { return }
            let scanningResultViewController = Storyboards.scanningResultStoryboard.instantiateViewController(withIdentifier: Constants.scanningResultID.rawValue) as! ScanningResultViewController
            scanningResultViewController.setContent(type: .see, image: image, card: card)
            scanningResultViewController.isFirstTime = false
            mSelf.navigationController?.pushViewController(scanningResultViewController, animated: true)
        }
        return cell
    }
}

extension ListOfCardsViewController: CLImageEditorDelegate {
    func imageEditor(_ editor: CLImageEditor!, didFinishEditingWith image: UIImage!) {
        textRecognizer(image: image)
        isReset = false
        editor.dismiss(animated: true, completion: nil)
    }
    
    func imageEditorDidCancel(_ editor: CLImageEditor!) {
        isReset = false
        editor.dismiss(animated: true, completion: nil)
    }
}

extension ListOfCardsViewController: UITextFieldDelegate {
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let content = (textField.text ?? "").lowercased()
        listOfCards.removeAll()
        for element in listOfCardsTempt {
            let card = element.1
            let name = (card.name ?? "").lowercased()
            let mobile = (card.mobile ?? "").lowercased()
            let email = (card.email ?? "").lowercased()
            let organization = (card.organization ?? "").lowercased()
            if name.contains(content) || mobile.contains(content) || email.contains(content) || organization.contains(content) {
                listOfCards.append((UIImage.fontAwesomeIcon(name: .addressCard, style: .solid, textColor: AppColors.red, size: CGSize(width: 800, height: 400)), card))
            }
        }
        self.cardTableView.reloadData()
        textField.resignFirstResponder()
        return true
    }
}
