//
//  Constants.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit

enum Constants: String {
    case listOfCardsName = "ListOfCards"
    case listOfCardsID = "ListOfCardsID"
    
    case scanningResultName = "ScanningResult"
    case scanningResultID = "ScanningResultID"
    
    case editScanningResultName = "EditScanningResult"
    case editScanningResultID = "EditScanningResultID"
    
    case login = "Login"
    case loginID = "LoginID"
    case signUpID = "SignUpID"
    
    case qrCodeName = "QRCode"
    case qrCodeID = "QRCodeID"
    case qrCodeImageID = "QRCodeImageID"
    
    case tabbarName = "Tabbar"
    case tabbarID = "TabbarID"
    
    case confirmNewCardsName = "ConfirmNewCards"
    case confirmNewCardsID = "ConfirmNewCardsID"
    
    case settingName = "Setting"
    case settingID = "SettingID"
}

enum AppColors {
    static let red = UIColor(red: 199/255, green: 51/255, blue: 48/255, alpha: 1)
}

enum URLs: String {
    case localhost = "http://192.168.0.103:3000"
    case host = "https://scanning-business-card.herokuapp.com"
}

enum Storyboards {
    static let scanningResultStoryboard = UIStoryboard(name: Constants.scanningResultName.rawValue, bundle: nil)
    
    static let editScanningResultStoryboard = UIStoryboard(name: Constants.editScanningResultName.rawValue, bundle: nil)
}
