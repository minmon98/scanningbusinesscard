//
//  SignUpRepositoryImp.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/30/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import Alamofire

class SignUpRepositoryImp: SignUpRepository {
    func getSignUpStatus(username: String, password: String, completion: @escaping (Error?, LoginResponse?) -> Void) {
        let url = URLs.host.rawValue + "/signup"
        let params: Parameters = [
            "username": username,
            "password": password
        ]
        let getSignUStatusApi = API<LoginResponseEntity>(
            url: url,
            method: .post,
            params: params
        )
        getSignUStatusApi.request { (error, loginResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let loginResponseEntity = loginResponseEntity else { return }
                completion(nil, loginResponseEntity.mapToEM(param: loginResponseEntity))
            }
        }
    }
}
