//
//  CardRepositoryImp.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

class CardRepositoryImp: CardRepository {
    func mergeCards(cards: [Card], completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        let url = URLs.host.rawValue + "/merge-cards"
        let username = UserDefaults.standard.string(forKey: "__username__") ?? ""
        let params: Parameters = [
            "username": username,
            "cards": cards.map({ (card) -> Parameters in
                let cardJSON: Parameters = [
                    "cardId": card.cardId ?? "",
                    "name": card.name ?? "",
                    "title": card.title ?? "",
                    "organization": card.organization ?? "",
                    "mobile": card.mobile ?? "",
                    "email": card.email ?? ""
                ]
                return cardJSON
            })
        ]
        let mergeCardApi = API<GetAllCardsResponseEntity>(
            url: url,
            method: .post,
            params: params
        )
        mergeCardApi.request { (error, getAllCardsResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let getAllCardsResponseEntity = getAllCardsResponseEntity else { return }
                completion(nil, getAllCardsResponseEntity.mapToEM(param: getAllCardsResponseEntity))
            }
        }
    }
    
    func newCard(username: String, card: Card, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        let url = URLs.host.rawValue + "/cards"
        let params: Parameters = [
            "username": username,
            "cardId": card.cardId ?? "",
            "name": card.name ?? "",
            "title": card.title ?? "",
            "organization": card.organization ?? "",
            "mobile": card.mobile ?? "",
            "email": card.email ?? ""
        ]
        let createNewCardApi = API<GetAllCardsResponseEntity>(
            url: url,
            method: .post,
            params: params
        )
        createNewCardApi.request { (error, getAllCardsResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let getAllCardsResponseEntity = getAllCardsResponseEntity else { return }
                completion(nil, getAllCardsResponseEntity.mapToEM(param: getAllCardsResponseEntity))
            }
        }
    }
    
    func getCard(url: String, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        let getAllCardsApi = API<GetAllCardsResponseEntity>(
            url: url,
            method: .get,
            params: [:]
        )
        getAllCardsApi.request { (error, getAllCardsResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let getAllCardsResponseEntity = getAllCardsResponseEntity else { return }
                completion(nil, getAllCardsResponseEntity.mapToEM(param: getAllCardsResponseEntity))
            }
        }
    }
    
    func deleteCard(cardId: String, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        let url = URLs.host.rawValue + "/cards/?cardId=\(cardId)&username=\(UserDefaults.standard.string(forKey: "__username__") ?? "")"
        let deleteCardApi = API<GetAllCardsResponseEntity>(
            url: url,
            method: .delete,
            params: [:]
        )
        deleteCardApi.request { (error, getAllCardsResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let getAllCardsResponseEntity = getAllCardsResponseEntity else { return }
                completion(nil, getAllCardsResponseEntity.mapToEM(param: getAllCardsResponseEntity))
            }
        }
    }
    
    func updateCard(card: Card, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        let url = URLs.host.rawValue + "/cards"
        let params: Parameters = [
            "cardId": card.cardId ?? "",
            "name": card.name ?? "",
            "title": card.title ?? "",
            "organization": card.organization ?? "",
            "mobile": card.mobile ?? "",
            "email": card.email ?? ""
        ]
        let updateCardApi = API<GetAllCardsResponseEntity>(
            url: url,
            method: .put,
            params: params
        )
        updateCardApi.request { (error, getAllCardsResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let getAllCardsResponseEntity = getAllCardsResponseEntity else { return }
                completion(nil, getAllCardsResponseEntity.mapToEM(param: getAllCardsResponseEntity))
            }
        }
    }
    
    func generateQRCode(cardIds: [String], completion: @escaping (Error?, GenerateQRCodeResponse?) -> Void) {
        let url = URLs.host.rawValue + "/qrcode"
        let params: Parameters = [
            "cardIds": cardIds
        ]
        let generateQRCodeApi = API<GenerateQRCodeResponseEntity>(
            url: url,
            method: .post,
            params: params
        )
        generateQRCodeApi.request { (error, generateQRCodeResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let generateQRCodeResponseEntity = generateQRCodeResponseEntity else { return }
                completion(nil, generateQRCodeResponseEntity.mapToEM(param: generateQRCodeResponseEntity))
            }
        }
    }
    
    func getAllCards(username: String, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        let url = URLs.host.rawValue + "/all-cards/?username=\(username)"
        let getAllCardsApi = API<GetAllCardsResponseEntity>(
            url: url,
            method: .get,
            params: [:]
        )
        getAllCardsApi.request { (error, getAllCardsResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let getAllCardsResponseEntity = getAllCardsResponseEntity else { return }
                completion(nil, getAllCardsResponseEntity.mapToEM(param: getAllCardsResponseEntity))
            }
        }
    }
    
    func getExtractInformation(text: String, lines: [String], completion: @escaping (Error?, GetCardResponse?) -> Void) {
        let url = URLs.host.rawValue + "/extract-information"
        let params: Parameters = [
            "text": text,
            "lines": lines
        ]
        let getCardApi = API<GetCardResponseEntity>(
            url: url,
            method: .post,
            params: params
        )
        getCardApi.request { (error, cardEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let cardEntity = cardEntity else { return }
                completion(nil, cardEntity.mapToEM(param: cardEntity))
            }
        }
    }
}
