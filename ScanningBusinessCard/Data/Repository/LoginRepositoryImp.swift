//
//  LoginRepositoryImp.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/29/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import Alamofire

class LoginRepositoryImp: LoginRepository {
    func getLoginStatus(username: String, password: String, completion: @escaping (Error?, LoginResponse?) -> Void) {
        let url = URLs.host.rawValue + "/login"
        let params: Parameters = [
            "username": username,
            "password": password
        ]
        let getLoginStatusApi = API<LoginResponseEntity>(
            url: url,
            method: .post,
            params: params
        )
        getLoginStatusApi.request { (error, loginResponseEntity) in
            if error != nil {
                completion(error, nil)
            } else {
                guard let loginResponseEntity = loginResponseEntity else { return }
                completion(nil, loginResponseEntity.mapToEM(param: loginResponseEntity))
            }
        }
    }
}
