//
//  API.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import Alamofire
import ObjectMapper
import AlamofireObjectMapper
import SVProgressHUD

class API<T: Mappable> {
    var url: String?
    var method: HTTPMethod?
    var params: Parameters?
    
    init(url: String, method: HTTPMethod, params: Parameters) {
        self.url = url
        self.method = method
        self.params = params
    }
    
    func request(completion: @escaping (Error?, T?) -> Void) {
        SVProgressHUD.setForegroundColor(AppColors.red)
        SVProgressHUD.setBackgroundColor(UIColor.black)
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.show(withStatus: "Đang phân tích")
        
        guard
            let url = url,
            let method = method,
            let params = params
        else { return }
        
        switch method {
            case .post, .delete, .put:
                Alamofire.request(url, method: method, parameters: params, encoding: JSONEncoding.default, headers: [:]).responseObject { (response: DataResponse<T>) in
                    guard let value = response.result.value else {
                        SVProgressHUD.dismiss()
                        completion(AppError(content: "Lỗi server"), nil)
                        return
                    }
                    completion(nil, value)
                }
            default:
                Alamofire.request(url).responseObject { (response: DataResponse<T>) in
                    guard let value = response.result.value else {
                        SVProgressHUD.dismiss()
                        completion(AppError(content: "Lỗi server"), nil)
                        return
                    }
                    completion(nil, value)
                }
        }
    }
}

class AppError: Error {
    var content: String?
    
    init(content: String) {
        self.content = content
    }
}
