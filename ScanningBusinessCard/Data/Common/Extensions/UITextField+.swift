//
//  UITextField+.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 2/2/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import Then

extension UITextField {
    func config() {
        self.layer.do {
            $0.cornerRadius = 5
            $0.borderWidth = 1
            $0.borderColor = UIColor.gray.cgColor
            $0.masksToBounds = true
        }
    }
}
