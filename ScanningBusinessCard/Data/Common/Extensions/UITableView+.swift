//
//  UITableView+.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 2/3/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit

extension UITableView {
    func scrollToBottom(array: [Any]) {
        let indexPath = IndexPath(row: array.count - 1, section: 0)
        self.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
}
