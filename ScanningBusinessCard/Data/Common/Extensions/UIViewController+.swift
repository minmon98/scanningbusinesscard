//
//  UIViewController+.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 2/2/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit

extension UIViewController {
    func showError(content: String) {
        let controller = UIAlertController(title: "Thông báo", message: content, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        controller.addAction(action)
        controller.view.tintColor = AppColors.red
        self.present(controller, animated: true, completion: nil)
    }
}
