//
//  Mapper.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

protocol Mapper {
    associatedtype DM
    associatedtype EM
    
    func mapToEM(param: DM) -> EM
}
