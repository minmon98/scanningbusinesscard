//
//  GetCardResponseEntity.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import ObjectMapper

class GetCardResponseEntity: Mappable {
    var success: Bool?
    var result: CardEntity?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        success <- map["success"]
        result  <- map["result"]
    }
}

extension GetCardResponseEntity: Mapper {
    typealias DM = GetCardResponseEntity
    
    typealias EM = GetCardResponse
    
    func mapToEM(param: GetCardResponseEntity) -> GetCardResponse {
        guard
            let success = param.success,
            let result = param.result
        else { return GetCardResponse(success: true, result: Card(
            cardId: "",
            name: "",
            title: "",
            organization: "",
            mobile: "",
            email: ""
        ))}
        
        return GetCardResponse(
            success: success,
            result: result.mapToEM(param: result)
        )
    }
}
