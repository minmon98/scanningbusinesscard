//
//  GetAllCardsResponseEntity.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/1/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import ObjectMapper

class GetAllCardsResponseEntity: Mappable {
    var success: Bool?
    var message: String?
    var cards: [CardEntity]?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
        cards   <- map["cards"]
    }
}

extension GetAllCardsResponseEntity: Mapper {
    typealias DM = GetAllCardsResponseEntity
    
    typealias EM = GetAllCardsResponse
    
    func mapToEM(param: GetAllCardsResponseEntity) -> GetAllCardsResponse {
//        guard
//            let success = param.success,
//            let message = param.message,
//            let cards = param.cards
//        else {
//            return GetAllCardsResponse(
//                success: false,
//                message: "",
//                cards: [Card(
//                    name: "",
//                    title: "",
//                    organization: "",
//                    mobile: "",
//                    email: ""
//                )]
//            )
//        }
        
        if let success = success {
            if success {
                if let cards = cards {
                    return GetAllCardsResponse(
                        success: success,
                        message: "",
                        cards: cards.map({ return $0.mapToEM(param: $0) })
                    )
                }
            } else {
                if let message = message {
                    return GetAllCardsResponse(
                        success: success,
                        message: message,
                        cards: []
                    )
                }
            }
        }
        return GetAllCardsResponse(
            success: success ?? true,
            message: message ?? "",
            cards: [Card(
                cardId: "",
                name: "",
                title: "",
                organization: "",
                mobile: "",
                email: ""
            )]
        )
    }
}

