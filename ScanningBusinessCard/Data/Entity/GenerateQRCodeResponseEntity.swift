//
//  GenerateQRCodeResponse.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import ObjectMapper

class GenerateQRCodeResponseEntity: Mappable {
    var success: Bool?
    var url: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        success <- map["success"]
        url     <- map["url"]
    }
}

extension GenerateQRCodeResponseEntity: Mapper {
    typealias DM = GenerateQRCodeResponseEntity
    
    typealias EM = GenerateQRCodeResponse
    
    func mapToEM(param: GenerateQRCodeResponseEntity) -> GenerateQRCodeResponse {
        return GenerateQRCodeResponse(success: self.success ?? true, url: self.url ?? "")
    }
}

