//
//  LoginResponseEntity.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/29/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import ObjectMapper

class LoginResponseEntity: Mappable {
    var success: Bool?
    var message: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        success <- map["success"]
        message <- map["message"]
    }
}

extension LoginResponseEntity: Mapper {
    typealias DM = LoginResponseEntity
    
    typealias EM = LoginResponse
    
    func mapToEM(param: LoginResponseEntity) -> LoginResponse {
        return LoginResponse(success: self.success ?? true, message: self.message ?? "")
    }
}
