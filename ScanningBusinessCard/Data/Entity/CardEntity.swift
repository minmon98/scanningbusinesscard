//
//  CardEntity.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import ObjectMapper

class CardEntity: Mappable {
    var cardId: String?
    var name: String?
    var title: String?
    var organization: String?
    var mobile: String?
    var email: String?
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        cardId              <- map["cardId"]
        name                <- map["name"]
        title               <- map["title"]
        organization        <- map["organization"]
        mobile              <- map["mobile"]
        email               <- map["email"]
    }
}

extension CardEntity: Mapper {
    typealias DM = CardEntity
    
    typealias EM = Card
    
    func mapToEM(param: CardEntity) -> Card {
        return Card(
            cardId: param.cardId ?? "",
            name: param.name ?? "",
            title: param.title ?? "",
            organization: param.organization ?? "",
            mobile: param.mobile ?? "",
            email: param.email ?? ""
        )
    }
}
