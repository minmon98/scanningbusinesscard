//
//  AppDelegate.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/11/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        let username = UserDefaults.standard.string(forKey: "__username__") ?? ""
        if username == "" {
            let loginViewController = UIStoryboard(name: Constants.login.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.loginID.rawValue)
            window?.rootViewController = loginViewController
        } else {
            let tabbarViewController = UIStoryboard(name: Constants.tabbarName.rawValue, bundle: nil).instantiateViewController(withIdentifier: Constants.tabbarID.rawValue)
            window?.rootViewController = tabbarViewController
        }
        
        return true
    }
}

