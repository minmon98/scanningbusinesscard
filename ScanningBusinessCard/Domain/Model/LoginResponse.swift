//
//  LoginResponse.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/29/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class LoginResponse {
    var success: Bool?
    var message: String?
    
    init(success: Bool, message: String) {
        self.success = success
        self.message = message
    }
}
