//
//  GetAllCardsResponse.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/1/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class GetAllCardsResponse {
    var success: Bool?
    var message: String?
    var cards: [Card]?
    
    init(success: Bool, message: String, cards: [Card]) {
        self.success = success
        self.message = message
        self.cards = cards
    }
}
