//
//  GetCardResponse.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class GetCardResponse {
    var success: Bool?
    var result: Card?
    
    init(success: Bool, result: Card) {
        self.success = success
        self.result = result
    }
}
