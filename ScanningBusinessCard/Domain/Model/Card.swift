//
//  Card.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class Card {
    var cardId: String?
    var name: String?
    var title: String?
    var organization: String?
    var mobile: String?
    var email: String?
    
    init(cardId: String, name: String, title: String, organization: String, mobile: String, email: String) {
        self.cardId = cardId
        self.name = name
        self.title = title
        self.organization = organization
        self.mobile = mobile
        self.email = email
    }
}
