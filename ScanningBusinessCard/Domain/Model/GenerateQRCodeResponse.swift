//
//  GenerateQRCodeReponse.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class GenerateQRCodeResponse {
    var success: Bool?
    var url: String?
    
    init(success: Bool, url: String) {
        self.success = success
        self.url = url
    }
}
