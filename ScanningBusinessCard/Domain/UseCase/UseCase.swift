//
//  UseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

protocol UseCase {
    associatedtype Param
    associatedtype Output
    
    func excute(param: Param) -> Output
}

protocol UseCaseWithoutParam {
    associatedtype Output
    
    func excute() -> Output
}
