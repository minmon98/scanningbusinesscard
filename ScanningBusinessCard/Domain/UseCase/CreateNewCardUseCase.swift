//
//  CreateNewCardUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/6/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import Foundation

class CreateNewCardUseCase: UseCase {
    typealias Param = CreateNewCardParam
    
    typealias Output = Void
    
    func excute(param: CreateNewCardParam) -> Void {
        guard
            let username = UserDefaults.standard.string(forKey: "__username__"),
            let card = param.card
        else { return }
        CardRepositoryImp().newCard(
            username: username,
            card: card) {
                param.completion($0, $1)
        }
    }
}

class CreateNewCardParam {
    var card: Card?
    var completion: ((Error?, GetAllCardsResponse?) -> Void)
    
    init(card: Card, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        self.card = card
        self.completion = completion
    }
}
