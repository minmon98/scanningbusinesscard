//
//  GetExtractInformationUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/27/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class GetExtractInformationUseCase: UseCase {
    typealias Param = GetExtractInformationParam
    
    typealias Output = Void
    
    func excute(param: GetExtractInformationParam) -> Void {
        guard
            let text = param.text,
            let lines = param.lines
        else { return }
        CardRepositoryImp().getExtractInformation(text: text, lines: lines) {
            param.completion($0, $1)
        }
    }
}

class GetExtractInformationParam {
    var text: String?
    var lines: [String]?
    var completion: ((Error?, GetCardResponse?) -> Void)
    
    init(text: String, lines: [String], completion: @escaping (Error?, GetCardResponse?) -> Void) {
        self.text = text
        self.lines = lines
        self.completion = completion
    }
}
