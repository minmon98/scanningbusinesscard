//
//  GetAllCardsUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/1/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class GetAllCardsUseCase: UseCase {
    typealias Param = GetAllCardsParam
    
    typealias Output = Void
    
    func excute(param: GetAllCardsParam) -> Void {
        guard let username = param.username else { return }
        CardRepositoryImp().getAllCards(username: username) {
            param.completion($0, $1)
        }
    }
}

class GetAllCardsParam {
    var username: String?
    var completion: ((Error?, GetAllCardsResponse?) -> Void)
    
    init(username: String, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        self.username = username
        self.completion = completion
    }
}
