//
//  SignUpUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/30/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class SignUpUseCase: UseCase {
    typealias Param = LoginParam
    
    typealias Output = Void
    
    func excute(param: LoginParam) -> Void {
        guard
            let username = param.username,
            let password = param.password
        else { return }
        SignUpRepositoryImp().getSignUpStatus(
            username: username,
            password: password) {
                param.completion($0, $1)
        }
    }
}
