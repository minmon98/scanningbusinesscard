//
//  GenerateQRCodeUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class GenerateQRCodeUseCase: UseCase {
    typealias Param = GenerateQRCodeParam
    
    typealias Output = Void
    
    func excute(param: GenerateQRCodeParam) -> Void {
        guard let cardIds = param.cardIds else { return }
        CardRepositoryImp().generateQRCode(cardIds: cardIds) {
            param.completion($0, $1)
        }
    }
}

class GenerateQRCodeParam {
    var cardIds: [String]?
    var completion: ((Error?, GenerateQRCodeResponse?) -> Void)
    
    init(cardIds: [String], completion: @escaping (Error?, GenerateQRCodeResponse?) -> Void) {
        self.cardIds = cardIds
        self.completion = completion
    }
}
