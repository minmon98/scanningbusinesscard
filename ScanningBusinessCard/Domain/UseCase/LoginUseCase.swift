//
//  LoginUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/29/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class LoginUseCase: UseCase {
    typealias Param = LoginParam
    
    typealias Output = Void
    
    func excute(param: LoginParam) -> Void {
        guard
            let username = param.username,
            let password = param.password
        else { return }
        LoginRepositoryImp().getLoginStatus(
            username: username,
            password: password) {
                param.completion($0, $1)
        }
    }
}

class LoginParam {
    var username: String?
    var password: String?
    var completion: (((Error?, LoginResponse?) -> Void))
    
    init(username: String, password: String, completion: @escaping ((Error?, LoginResponse?) -> Void)) {
        self.username = username
        self.password = password
        self.completion = completion
    }
}
