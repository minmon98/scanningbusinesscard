//
//  DeleteCardUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class DeleteCardUseCase: UseCase {
    typealias Param = DeleteCardParam
    
    typealias Output = Void
    
    func excute(param: DeleteCardParam) -> Void {
        guard let cardId = param.cardId else { return }
        CardRepositoryImp().deleteCard(cardId: cardId) {
            param.completion($0, $1)
        }
    }
}

class DeleteCardParam {
    var cardId: String?
    var completion: ((Error?, GetAllCardsResponse?) -> Void)
    
    init(cardId: String, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        self.cardId = cardId
        self.completion = completion
    }
}
