//
//  MergeCardsUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/7/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

import Foundation

class MergeCardsUseCase: UseCase {
    typealias Param = MergeCardsParam
    
    typealias Output = Void
    
    func excute(param: MergeCardsParam) -> Void {
        guard let cards = param.cards else { return }
        CardRepositoryImp().mergeCards(cards: cards) {
            param.completion($0, $1)
        }
    }
}

class MergeCardsParam {
    var cards: [Card]?
    var completion: ((Error?, GetAllCardsResponse?) -> Void)
    
    init(cards: [Card], completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        self.cards = cards
        self.completion = completion
    }
}
