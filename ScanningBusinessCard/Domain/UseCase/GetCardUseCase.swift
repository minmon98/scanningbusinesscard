//
//  GetCardUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class GetCardUseCase: UseCase {
    typealias Param = GetCardParam
    
    typealias Output = Void
    
    func excute(param: GetCardParam) -> Void {
        guard let url = param.url else { return }
        CardRepositoryImp().getCard(url: url) {
            param.completion($0, $1)
        }
    }
}

class GetCardParam {
    var url: String?
    var completion: ((Error?, GetAllCardsResponse?) -> Void)
    
    init(url: String, completion: @escaping ((Error?, GetAllCardsResponse?) -> Void)) {
        self.url = url
        self.completion = completion
    }
}
