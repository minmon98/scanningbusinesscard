//
//  UpdateCardUseCase.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 4/5/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

class UpdateCardUseCase: UseCase {
    typealias Param = UpdateCardParam
    
    typealias Output = Void
    
    func excute(param: UpdateCardParam) -> Void {
        guard let card = param.card else { return }
        CardRepositoryImp().updateCard(card: card) {
            param.completion($0, $1)
        }
    }
}

class UpdateCardParam {
    var card: Card?
    var completion: ((Error?, GetAllCardsResponse?) -> Void)
    
    init(card: Card, completion: @escaping (Error?, GetAllCardsResponse?) -> Void) {
        self.card = card
        self.completion = completion
    }
}
