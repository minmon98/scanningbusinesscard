//
//  LoginRepository.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/29/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

protocol LoginRepository {
    func getLoginStatus(username: String, password: String, completion: @escaping (Error?, LoginResponse?) -> Void)
}
