//
//  SignUpRepository.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 3/30/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

protocol SignUpRepository {
    func getSignUpStatus(username: String, password: String, completion: @escaping (Error?, LoginResponse?) -> Void)
}
