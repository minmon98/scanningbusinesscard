//
//  CardRepository.swift
//  ScanningBusinessCard
//
//  Created by Minh Mon on 1/12/20.
//  Copyright © 2020 Minh Mon. All rights reserved.
//

protocol CardRepository {
    func getCard(url: String, completion: @escaping (Error?, GetAllCardsResponse?) -> Void)
    func getExtractInformation(text: String, lines: [String], completion: @escaping (Error?, GetCardResponse?) -> Void)
    func getAllCards(username: String, completion: @escaping (Error?, GetAllCardsResponse?) -> Void)
    func generateQRCode(cardIds: [String], completion: @escaping (Error?, GenerateQRCodeResponse?) -> Void)
    func mergeCards(cards: [Card], completion: @escaping (Error?, GetAllCardsResponse?) -> Void)
    func deleteCard(cardId: String, completion: @escaping (Error?, GetAllCardsResponse?) -> Void)
    func updateCard(card: Card, completion: @escaping (Error?, GetAllCardsResponse?) -> Void)
    func newCard(username: String, card: Card, completion: @escaping (Error?, GetAllCardsResponse?) -> Void)
}
